import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Question {

    //Attributs opération
    int num1 = 0;
    int num2 = 0;
    int min = 1;
    int max = 10;
    char operators[] = new char[]{'+', '-', '*'};
    char operator;
    int result;
    int answer;

    public Question() {

        //Définir num1 et num2
        this.num1 = defineNumber();
        this.num2 = defineNumber();
        this.operator = defineOperator();
        this.result = Mresult();

        //DEBUG
        //System.out.println(num1); System.out.println(operator);System.out.println(num2);
        //System.out.println(result);
    }

    //Méthode de définition d'un nombre aléatoire
    private int defineNumber() {
        int number = ThreadLocalRandom.current().nextInt(min, max + 1);
        return number;
    }

    private char defineOperator() {
        char res = operators[(int) (Math.random() * operators.length)];
        return res;
    }

    private int Mresult() {
        switch (operator) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '/':
                result = num1 / num2;
                break;
            case '*':
                result = num1 * num2;
                break;
        }
        return result;
    }

    public void AskQuestion() {
        String question = "quel est le résultat de : " + num1 + " " + operator + " " + num2 + " ?";
        System.out.println(question);
    }
    public void readAnswer(){
        Scanner sc=new Scanner(System.in);
        this.answer=sc.nextInt();
    }
    public void compareAnswer(){
        if (answer==result){
            System.out.println("Très bien!");}
            else{
                System.out.println("Faux! La réponse était "+result);
            }
    }

}
